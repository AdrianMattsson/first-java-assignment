import characters.errors.InvalidArmorException;
import characters.errors.InvalidLevelException;
import characters.errors.InvalidWeaponException;
import items.ItemSlot;
import characters.Mage;
import items.armor.Armor;
import items.armor.ArmorType;
import items.weapons.Weapon;
import items.weapons.WeaponType;

public class Main {
    public static void main(String[] args) throws InvalidArmorException, InvalidWeaponException, InvalidLevelException {
            Mage character =  new Mage("Gregory");

            Armor hat = new Armor("Hat of fortune",1,0,0,9, ArmorType.Cloth, ItemSlot.Head);
            Armor chestplate = new Armor("Chestplate of stone",0,3,4,5, ArmorType.Cloth, ItemSlot.Body);
            Armor pants = new Armor("Pants of swiftness",0,0,6,1, ArmorType.Cloth, ItemSlot.Legs);
            Weapon weapon = new Weapon("Staff of slayingggg",0,13,5, WeaponType.Staff, ItemSlot.Weapon);

            character.equip(hat);
            character.equip(chestplate);
            character.equip(pants);
            character.equip(weapon);

            character.levelUp();

            System.out.println(character.displayInformation());
            System.out.println(character.displayEquippedItems());
            }
}
