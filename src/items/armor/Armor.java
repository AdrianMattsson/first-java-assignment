package items.armor;

import items.ItemSlot;
import items.Item;

public class Armor extends Item {

    private ArmorType type;

    public Armor(String name, int requiredLevel, int addsStrength, int addsDexterity, int addsIntelligence, ArmorType type, ItemSlot itemSlot) {
        super(name, requiredLevel, addsStrength, addsDexterity, addsIntelligence, itemSlot);
        this.type = type;
    }

    @Override
    public ArmorType getType() {
        return type;
    }
}
