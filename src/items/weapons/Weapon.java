package items.weapons;

import items.ItemSlot;
import items.Item;

public class Weapon extends Item {

    private float damagePerSecond;
    private WeaponType type;

    public Weapon(String name, int requiredLevel, int damage, float attackSpeed, WeaponType type, ItemSlot itemSlot) {
        super(name, requiredLevel, itemSlot);
        this.type = type;
        damagePerSecond = damage * attackSpeed;
    }

    @Override
    public WeaponType getType() {
        return type;
    }

    public float getDamagePerSecond() {
        return damagePerSecond;
    }
}
