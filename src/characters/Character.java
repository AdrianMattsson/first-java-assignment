package characters;

import java.util.HashMap;
import characters.errors.InvalidArmorException;
import characters.errors.InvalidLevelException;
import characters.errors.InvalidWeaponException;
import items.Item;
import items.ItemSlot;
import items.armor.Armor;
import items.armor.ArmorType;
import items.weapons.Weapon;
import items.weapons.WeaponType;

public abstract class Character {
    private String name;
    private int level = 1;

    private WeaponType[] allowedWeapons = {};
    private ArmorType[] allowedArmor= {};
    private HashMap<ItemSlot, Item> equippedItems = new HashMap<>();     // Items equipped on the character, <Slot, Item>
    private PrimaryAttributes baseAttributes;     //Attributes of the character

    public Character(String name, PrimaryAttributes attributes) {
        this.name = name;
        baseAttributes = attributes;
    }

    //Levels up the character
    public void levelUp(int strengthIncrease, int dexterityIncrease, int intelligenceIncrease) {
        baseAttributes.addToAttributes(strengthIncrease, dexterityIncrease, intelligenceIncrease);
        level += 1;
    }

    //Counts the damage per second the character attacks with
    public float countDamage() {
        float tempDamage = 1;
        //Checks if any weapons are equipped. If not, it leaves the base damage at 1
        if (equippedItems.get(ItemSlot.Weapon) instanceof Weapon) {
            tempDamage = ((Weapon) equippedItems.get(ItemSlot.Weapon)).getDamagePerSecond();
        }
        //Sends it to be multiplied by special attributes before returning
        return getDamageMultiplier(tempDamage);
    }

    //Used by children to multiply damage with its special attribute
    public abstract float getDamageMultiplier(float damagePerSecond);

    public PrimaryAttributes getTotalAttributes() {
        PrimaryAttributes tempAttributes = new PrimaryAttributes();
        tempAttributes.addToAttributes(baseAttributes.getStrength(), baseAttributes.getDexterity(), baseAttributes.getIntelligence());
        equippedItems.forEach((slot, item) -> tempAttributes.addToAttributes(item.getAttributes().getStrength(), item.getAttributes().getDexterity(), item.getAttributes().getIntelligence()));
        return tempAttributes;
    }

    //Checks if the items required level is same or lower than the characters level
    public boolean checkRequiredLevel(Item item) throws InvalidLevelException {
        if (item.getRequiredLevel() <= level) {
            return true;
        } else {
            throw new InvalidLevelException("Character is not high enough level to equip this item.");
        }
    }

    //Method for equipping armor
    public void equip(Armor item) throws InvalidArmorException, InvalidLevelException {
        if(checkRequiredLevel(item)) {
            boolean allowed = false;

            for (int i = 0; i < allowedArmor.length; i++) {
                //If the armors type matches allowed types it equips
                if (allowedArmor[i].equals(item.getType())) {
                    equippedItems.put(item.getItemSlot(), item);
                    allowed = true;
                    break;
                }
            }
            if (!allowed) throw new InvalidArmorException("This type of armor cannot be equipped to this character.");
        }
    }

    //Method for equipping weapons
    public void equip(Weapon item) throws InvalidWeaponException, InvalidLevelException {
        if(checkRequiredLevel(item)) {
            boolean allowed = false;

            for (int i = 0; i < allowedWeapons.length; i++) {
                //If the weapon type matches allowed types it equips
                if (allowedWeapons[i].equals(item.getType())) {
                    equippedItems.put(item.getItemSlot(), item);
                    allowed = true;
                    break;
                }
            }
            if (!allowed) throw new InvalidWeaponException("This type of weapon cannot be equipped to this character.");
        }
    }

    //Display information about the current character
    //The attribute statistics is the total (base + gear bonus).
    public String displayInformation() {
        StringBuilder displayString = new StringBuilder();
        displayString.append("Name:" + name + "\n");
        displayString.append("Level:" + level + "\n");
        displayString.append("Strength:" + getTotalAttributes().getStrength() + "\n");
        displayString.append("Dexterity:" + getTotalAttributes().getDexterity() + "\n");
        displayString.append("Intelligence:" + getTotalAttributes().getIntelligence() + "\n");
        displayString.append("Damage:" + countDamage() + "\n");
        return displayString.toString();
    }

    //Displays information about all items equipped
    public String displayEquippedItems() {
        StringBuilder displayString = new StringBuilder();
        displayString.append("Head: " + (equippedItems.get(ItemSlot.Head) == null? "Empty" : equippedItems.get(ItemSlot.Head).getName()));
        displayString.append("\nBody: " + (equippedItems.get(ItemSlot.Body) == null? "Empty" : equippedItems.get(ItemSlot.Body).getName()));
        displayString.append("\nLegs: " + (equippedItems.get(ItemSlot.Legs) == null? "Empty" : equippedItems.get(ItemSlot.Legs).getName()));
        displayString.append("\nWeapon: " + (equippedItems.get(ItemSlot.Weapon) == null? "Empty" : equippedItems.get(ItemSlot.Weapon).getName()));
        return displayString.toString();
    }

    /*
     *
     * Standard getters and setters below
     *
     */

    //Returns the item equipped in the ItemSlot
    public Item getEquippedItem(ItemSlot slot) {return equippedItems.get(slot);}

    public void setAllowedWeapons(WeaponType[] allowedWeapons) {this.allowedWeapons = allowedWeapons;}

    public void setAllowedArmor(ArmorType[] allowedArmor) {this.allowedArmor = allowedArmor;}

    public String getName() {return name;}

    public int getLevel() {return level;}
}

