package characters;

public class PrimaryAttributes {
    private int strength = 0;
    private int dexterity = 0;
    private int intelligence = 0;

    public PrimaryAttributes() {
    }

    public PrimaryAttributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    //Adds the incoming attributes to the current attributes
    public void addToAttributes(int strength, int dexterity, int intelligence) {
        this.strength += strength;
        this.dexterity += dexterity;
        this.intelligence += intelligence;
    }

    //Overrides the current attributes to the incoming attributes
    public void setToAttributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    //Getters
    public int getStrength() {
        return this.strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }
}
