package characters.errors;

public class InvalidArmorException extends Exception{
    //Used when the chosen armor is not allowed to be equipped to the chosen character
    public InvalidArmorException(String errorMessage) {
        super(errorMessage);
    }
}
