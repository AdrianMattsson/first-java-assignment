package characters;

import items.armor.ArmorType;
import items.weapons.WeaponType;

public class Mage extends Character{

    public Mage (String name) {
        super(name, new PrimaryAttributes(1, 1, 8));
        //Allowed types of items to be equipped to this character. Other items will NOT be able to be equipped to this character
        setAllowedWeapons(new WeaponType[]{
                WeaponType.Staff,
                WeaponType.Wand});
        setAllowedArmor(new ArmorType[]{
                ArmorType.Cloth});
    }

    public void levelUp() {
        levelUp(1,1,5);
    }

    //Multiplies the characters damage with its special attribute
    @Override
    public float getDamageMultiplier(float damagePerSecond) {
        return damagePerSecond * (1 + getTotalAttributes().getIntelligence()/100f);
    }
}
