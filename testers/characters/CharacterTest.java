package characters;

import characters.errors.InvalidArmorException;
import characters.errors.InvalidLevelException;
import characters.errors.InvalidWeaponException;
import items.Item;
import items.ItemSlot;
import items.armor.Armor;
import items.armor.ArmorType;
import items.weapons.Weapon;
import items.weapons.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    ////////////////////////////////////////// CHARACTER LEVELS ////////////////////////////////////////////////////////
    @Test
    void TestBaseLevel_Characters_ShouldBeLevelOne() {
        Mage mage = new Mage("Greg");
        Ranger ranger = new Ranger("Gregory");
        Rogue rogue = new Rogue("Gregster");
        Warrior warrior = new Warrior("Greggy");

        int[] levels = {mage.getLevel(), ranger.getLevel(), rogue.getLevel(), warrior.getLevel()};
        int[] expected = {1,1,1,1};

        assertArrayEquals(expected, levels);
    }

    @Test
    void TestLevelUp_Characters_ShouldBeLevelTwo() {
        Mage mage = new Mage("Greg");
        Ranger ranger = new Ranger("Gregory");
        Rogue rogue = new Rogue("Gregster");
        Warrior warrior = new Warrior("Greggy");
        mage.levelUp();
        ranger.levelUp();
        rogue.levelUp();
        warrior.levelUp();

        int[] levels = {mage.getLevel(), ranger.getLevel(), rogue.getLevel(), warrior.getLevel()};
        int[] expected = {2,2,2,2};

        assertArrayEquals(expected, levels);
    }

    @Test
    void TestBaseLevel_Mage_ShouldHaveBaseAttributes() {
        Mage character = new Mage("Gregory");

        int[] attributes = {character.getTotalAttributes().getStrength(), character.getTotalAttributes().getDexterity(), character.getTotalAttributes().getIntelligence()};
        int[] expected = {1,1,8};

        assertArrayEquals(expected, attributes);
    }

    @Test
    void TestBaseLevel_Ranger_ShouldHaveBaseAttributes() {
        Ranger character = new Ranger("Gregory");

        int[] attributes = {character.getTotalAttributes().getStrength(), character.getTotalAttributes().getDexterity(), character.getTotalAttributes().getIntelligence()};
        int[] expected = {1,7,1};

        assertArrayEquals(expected, attributes);
    }

    @Test
    void TestBaseLevel_Rogue_ShouldHaveBaseAttributes() {
        Rogue character = new Rogue("Gregory");

        int[] attributes = {character.getTotalAttributes().getStrength(), character.getTotalAttributes().getDexterity(), character.getTotalAttributes().getIntelligence()};
        int[] expected = {2,6,1};

        assertArrayEquals(expected, attributes);
    }

    @Test
    void TestBaseLevel_Warrior_ShouldHaveBaseAttributes() {
        Warrior character = new Warrior("Gregory");

        int[] attributes = {character.getTotalAttributes().getStrength(), character.getTotalAttributes().getDexterity(), character.getTotalAttributes().getIntelligence()};
        int[] expected = {5,2,1};

        assertArrayEquals(expected, attributes);
    }

    @Test
    void TestLevelUp_Mage_ShouldIncreaseAttributes() {
        Mage character = new Mage("Gregory");
        character.levelUp();

        int level = character.getLevel();
        int[] attributes = {character.getTotalAttributes().getStrength(), character.getTotalAttributes().getDexterity(), character.getTotalAttributes().getIntelligence()};
        int[] expected = {2,2,13};

        assertEquals(2, level);
        assertArrayEquals(expected, attributes);
    }

    @Test
    void TestLevelUp_Ranger_ShouldIncreaseAttributes() {
        Ranger character = new Ranger("Gregory");
        character.levelUp();

        int[] attributes = {character.getTotalAttributes().getStrength(), character.getTotalAttributes().getDexterity(), character.getTotalAttributes().getIntelligence()};
        int[] expected = {2,12,2};

        assertArrayEquals(expected, attributes);
    }

    @Test
    void TestLevelUp_Rogue_ShouldIncreaseAttributes() {
        Rogue character = new Rogue("Gregory");
        character.levelUp();

        int[] attributes = {character.getTotalAttributes().getStrength(), character.getTotalAttributes().getDexterity(), character.getTotalAttributes().getIntelligence()};
        int[] expected = {3,10,2};

        assertArrayEquals(expected, attributes);
    }

    @Test
    void TestLevelUp_Warrior_ShouldIncreaseAttributes() {
        Warrior character = new Warrior("Gregory");
        character.levelUp();

        int[] attributes = {character.getTotalAttributes().getStrength(), character.getTotalAttributes().getDexterity(), character.getTotalAttributes().getIntelligence()};
        int[] expected = {8,4,2};

        assertArrayEquals(expected, attributes);
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////// COUNT DAMAGE ////////////////////////////////////////////////////////
    @Test
    void TestCountDamage_NoArmorNoWeapon_ShouldCountBaseDamage() {
        Warrior warrior = new Warrior("Bob");

        float expectedValue = 1*(1+(5/100f));
        float damage = warrior.countDamage();

        assertEquals(expectedValue, damage);
    }

    @Test
    void TestCountDamage_NoArmorWithWeapon_ShouldCountWeaponDamage() throws InvalidArmorException, InvalidWeaponException, InvalidLevelException {
        Weapon weapon = new Weapon("sword of slayinggg", 0, 3, 5, WeaponType.Sword, ItemSlot.Weapon);
        Warrior warrior = new Warrior("Bob");
        warrior.equip(weapon);

        double expectedValue = weapon.getDamagePerSecond()*(1+5f/100f);
        float damage = warrior.countDamage();

        assertEquals(expectedValue, damage);
    }

    @Test
    void TestCountDamage_WithArmorNoWeapon_ShouldCountWeaponDamage() throws InvalidArmorException, InvalidWeaponException, InvalidLevelException {
        Armor armor = new Armor("chestplate of leather", 0, 4, 4, 4, ArmorType.Mail, ItemSlot.Body);
        Warrior warrior = new Warrior("Bob");
        warrior.equip(armor);

        double expectedValue = 1*(1+9f/100f);
        float damage = warrior.countDamage();

        assertEquals(expectedValue, damage);    }

    @Test
    void TestCountDamage_WithArmorWithWeapon_ShouldCountWeaponDamage() throws InvalidArmorException, InvalidWeaponException, InvalidLevelException {
        Armor armor = new Armor("chestplate of leather", 0, 4, 4, 4, ArmorType.Mail, ItemSlot.Body);
        Weapon weapon = new Weapon("sword of slayinggg", 0, 3, 5, WeaponType.Sword, ItemSlot.Weapon);
        Warrior warrior = new Warrior("Bob");
        warrior.equip(armor);
        warrior.equip(weapon);

        double expectedValue = 15*(1+9f/100f);
        float damage = warrior.countDamage();

        assertEquals(expectedValue, damage);
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////// EQUIP ITEMS ////////////////////////////////////////////////////////
    @Test
    void TestEquipArmor_TooLowLevel_ShouldThrowException() {
        Armor armor = new Armor("chestplate of leather", 5, 4, 4, 4, ArmorType.Leather, ItemSlot.Body);
        Ranger ranger = new Ranger("Bob");

        assertThrows(InvalidLevelException.class, () -> ranger.equip(armor));
    }

    @Test
    void TestEquip_HighEnoughLevel_ShouldEquip() throws InvalidArmorException, InvalidWeaponException, InvalidLevelException {
        Armor armor = new Armor("chestplate of leather", 0, 4, 4, 4, ArmorType.Leather, ItemSlot.Body);
        Ranger ranger = new Ranger("Bob");
        ranger.equip(armor);

        Item equippedArmor = ranger.getEquippedItem(armor.getItemSlot());

        assertEquals(armor, equippedArmor);
    }

    @Test
    void TestEquip_WrongArmorType_ShouldThrowException() {
        Armor armor = new Armor("chestplate of leather", 0, 4, 4, 4, ArmorType.Leather, ItemSlot.Body);
        Warrior warrior = new Warrior("Bob");


        assertThrows(InvalidArmorException.class, () -> warrior.equip(armor));
    }

    @Test
    void TestEquip_RightArmorType_ShouldEquip() throws InvalidArmorException, InvalidWeaponException, InvalidLevelException {
        Armor armor = new Armor("chestplate of leather", 0, 4, 4, 4, ArmorType.Plate, ItemSlot.Body);
        Warrior warrior = new Warrior("Bob");
        warrior.equip(armor);

        Item equippedArmor = warrior.getEquippedItem(armor.getItemSlot());

        assertEquals(armor, equippedArmor);
    }

    @Test
    void TestEquip_WrongWeaponType_ShouldThrowException() {
        Weapon weapon = new Weapon("sword of slayinggg", 0, 3, 5, WeaponType.Wand, ItemSlot.Weapon);
        Warrior warrior = new Warrior("Bob");

        assertThrows(InvalidWeaponException.class, () -> warrior.equip(weapon));
    }

    @Test
    void TestEquip_RightWeaponType_ShouldEquip() throws InvalidArmorException, InvalidWeaponException, InvalidLevelException {
        Weapon weapon = new Weapon("sword of slayinggg", 0, 3, 5, WeaponType.Sword, ItemSlot.Weapon);
        Warrior warrior = new Warrior("Bob");
        warrior.equip(weapon);

        Item equippedWeapon = warrior.getEquippedItem(weapon.getItemSlot());

        assertEquals(weapon, equippedWeapon);
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////
}